package com.braciakoala.kinoclient.UI;

import com.braciakoala.kinoclient.*;
import com.braciakoala.kinoclient.model.Film;
import com.braciakoala.kinoclient.model.Screening;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * Klasa reprezentująca okno wyboru seansu.
 */
public class Search extends JFrame {
    /**
     * Lista wszystkich seansów
     */
    static ArrayList<Screening> seanse = new ArrayList<>();

    /**
     * Lista seansów spełniających kryteria
     */
    static ArrayList<Screening> aktualne = new ArrayList<>();

    /**
     * Wybrany seans, przypisywany po zatwierdzeniu wyboru
     */
    static Screening seans = new Screening();

    /**
     * Indeks aktualnego seansu na liście wszystkich seansów
     */
    static int seansIndex = 0;

    /**
     * zmienna pozwalająca określić, czy program jest w trakcie uruchamiania się
     */
    static boolean starting = true;

    /**
     * zmienna pozwalająca określić, czy klient jest połączony z serwerem
     */
    static boolean connected = true;

    /**
     * Metoda odświeżająca seanse - pobiera ich listę z serwera
     */
    public static void refreshSeanse(){
        try {
            seanse = Connection.getAllSeanse();
            seans = seanse.get(seansIndex);
            connected = true;
        } catch (IOException e) {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f,"Błąd połączenia z serwerem!", "Krytyczny błąd", JOptionPane.ERROR_MESSAGE);
            connected = false;
        } catch (IndexOutOfBoundsException e){
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f,"Błąd połączenia z serwerem!", "Krytyczny błąd", JOptionPane.ERROR_MESSAGE);
            if (starting) System.exit(0);
            else connected = false;
        }
    }

    /**
     * Konstruktor - tworzy nowe okno typu Search
     */
    public Search(){
        seansIndex = 0;
        seans = null;
        refreshSeanse();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        getContentPane().setLayout(null);
        GridBagConstraints c = new GridBagConstraints();
        setSize(1000,850);

        JPanel searchData = new JPanel();
        searchData.setLayout(new GridBagLayout());
        searchData.setBounds(0,0,300,200);
        getContentPane().add(searchData);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        buttonPanel.setMaximumSize(new Dimension(275,1000));
        JScrollPane buttons = new JScrollPane(buttonPanel);
        buttons.setBounds(0,200,300,600);
        buttons.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        buttons.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        buttons.setPreferredSize(new Dimension(275,600));
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 2;
        c.gridheight = 6;
        c.gridwidth = 3;
        c.weighty = 0.0;
        c.weightx = 0.0;
        add(buttons);

        //PRAWA STRONA

        JTextArea desc = new JTextArea();
        desc.setLineWrap(true);
        desc.setWrapStyleWord(true);
        desc.setEditable(false);
        desc.setMargin( new Insets(10,10,10,10) );
        JScrollPane descriptionPan = new JScrollPane(desc);
        descriptionPan.setBounds(300,0,700,700);
        descriptionPan.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        descriptionPan.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        descriptionPan.setPreferredSize(new Dimension(700,500));
        descriptionPan.setMinimumSize(new Dimension(700,500));
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 3;
        c.gridy = 0;
        c.gridheight = 7;
        c.gridwidth = 7;
        c.weighty = 0.0;
        c.weightx = 0.0;

        add(descriptionPan);

        JPanel acceptPan = new JPanel(new BorderLayout());
        acceptPan.setMinimumSize(new Dimension(700,100));
        acceptPan.setBounds(300,700,700,100);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 3;
        c.gridy = 7;
        c.gridheight = 1;
        c.gridwidth = 7;
        c.weighty = 0.0;
        c.weightx = 0.0;
        add(acceptPan);

        JLabel dayLabel = new JLabel("Dzień");
        dayLabel.setHorizontalAlignment(JTextField.CENTER);
        dayLabel.setSize(50,50);
        dayLabel.setBackground(Color.LIGHT_GRAY);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 0;
        c.ipady = 50;

        searchData.add(dayLabel, c);

        JLabel monthLabel = new JLabel("Miesiąc");
        monthLabel.setHorizontalAlignment(JTextField.CENTER);
        monthLabel.setBackground(Color.LIGHT_GRAY);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 0;
        c.ipady = 50;

        searchData.add(monthLabel, c);

        JLabel yearLabel = new JLabel("Rok");
        yearLabel.setHorizontalAlignment(JTextField.CENTER);
        yearLabel.setBackground(Color.LIGHT_GRAY);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 0;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 0;
        c.ipady = 50;

        searchData.add(yearLabel, c);

        JTextField dayField = new JTextField("");
        dayField.setHorizontalAlignment(JTextField.CENTER);
        dayField.setSize(50,50);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 100;
        c.ipady = 50;

        searchData.add(dayField, c);

        JTextField monthField = new JTextField("");
        monthField.setHorizontalAlignment(JTextField.CENTER);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 50;
        c.ipady = 50;

        searchData.add(monthField, c);

        JTextField yearField = new JTextField("");
        yearField.setHorizontalAlignment(JTextField.CENTER);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 1;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 50;
        c.ipady = 50;

        searchData.add(yearField, c);

        String[] list = {"Dowolny", "Akcji", "Romantyczny", "Wojenny", "Fantasy", "SF"};
        JComboBox genres = new JComboBox(list);
        ((JLabel)genres.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 2;
        c.gridheight = 1;
        c.gridwidth = 2;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 100;
        c.ipady = 50;

        searchData.add(genres, c);

        JButton search = new JButton("Szukaj");
        yearField.setHorizontalAlignment(JTextField.CENTER);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 2;
        c.gridheight = 1;
        c.gridwidth = 1;
        c.weightx = 0.0;
        c.weighty = 0.0;
        c.ipadx = 50;
        c.ipady = 50;

        searchData.add(search, c);

        search.addActionListener(ac0 -> {
            refreshSeanse();
            aktualne.clear();
            buttonPanel.removeAll();
            buttonPanel.revalidate();
            buttonPanel.repaint();
            boolean validDate = true;
            Calendar probe = Calendar.getInstance();
            try{
                int day = Integer.parseInt(dayField.getText());
                int month = Integer.parseInt(monthField.getText()) - 1;
                int year = Integer.parseInt(yearField.getText());
                probe.set(Calendar.DAY_OF_MONTH, day);
                probe.set(Calendar.MONTH, month);
                probe.set(Calendar.YEAR, year);
            }catch (Exception a){
                validDate = false;
            }
            int licznik = 0;

            String genre = genres.getSelectedItem().toString();

            for (Screening a: seanse) {
                boolean sameDay = probe.get(Calendar.YEAR) == a.getDate().get(Calendar.YEAR) && probe.get(Calendar.DAY_OF_YEAR) == a.getDate().get(Calendar.DAY_OF_YEAR);
                boolean properGenre = genre.equals("Dowolny") || genre.equals(a.getFilm().getGenre());
                if ((sameDay || !validDate) && properGenre) {
                    //System.out.println(a.toString());
                    SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                    aktualne.add(a);
                    String date = format1.format(a.getDate().getTime());
                    String text = date + " - " + a.getFilm().getTitle();
                    JButton button = new JButton(new String(text));
                    button.setName(Integer.toString(licznik));
                    button.addActionListener(ac1 -> {
                        JButton but = (JButton) ac1.getSource();
                        int index = Integer.parseInt(but.getName());
                        seans = aktualne.get(index);
                        System.out.println(seans);
                        System.out.println(seanse);
                        seansIndex = seans.getId() - 1;
                        System.out.println(seansIndex);
                        refreshSeanse();
                        desc.setText(seans.display());
                        JButton enter = new JButton("Dalej");
                        enter.addActionListener(ac2 -> {
                            refreshSeanse();
                            if (connected) {
                                Choose.choose = new Choose(seans);
                                setVisible(false);
                                dispose();
                            }
                        });
                        acceptPan.add(enter);
                        acceptPan.validate();
                    });
                    c.fill = GridBagConstraints.CENTER;
                    c.gridx = 0;
                    c.gridy = licznik;
                    c.gridheight = 1;
                    c.gridwidth = 1;
                    c.weighty = 1;
                    c.weightx = 1;
                    //c.ipadx = 80;
                    buttonPanel.add(button, c);
                    buttonPanel.validate();
                    licznik++;
                }
            }
        });

        setResizable(false);
        setVisible(true);
        starting = false;
    }
}