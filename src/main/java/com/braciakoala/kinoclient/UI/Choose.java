package com.braciakoala.kinoclient.UI;

import com.braciakoala.kinoclient.Connection;
import com.braciakoala.kinoclient.model.Order;
import com.braciakoala.kinoclient.model.Product;
import com.braciakoala.kinoclient.model.Screening;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Klasa reprezentująca okno finalizacji zamówienia - wyboru miejsc, przekąsek i wprowadzenia danych osobowych
 */
public class Choose extends JFrame {
    /**
     * Aktualny seans
     */
    static Screening seans;

    /**
     * Lista wybranych miejsc vipowskich
     */
    static ArrayList<Integer> listaVip = new ArrayList<>();

    /**
     * Lista wybranych miejsc normalnych
     */
    static ArrayList<Integer> listaNormal = new ArrayList<>();

    /**
     * Lista produktów - przekąsek i napojów
     */
    static ArrayList<Product> products = new ArrayList<>();

    /**
     * Lista spinnerów, zapełniana przy pobieraniu listy produktów
     */
    static ArrayList<JSpinner> spinners = new ArrayList<>();

    /**
     * Nasze okno typu Choose
     */
    static Choose choose;

    /**
     * Metoda odświeżająca wybrany seans
     */
    static void refresh(){
        Search.refreshSeanse();
        Choose.seans = null;
        Choose.seans = new Screening(Search.seans.getId(), Search.seans.getFilm(), Search.seans.getDate(), Search.seans.getNormalSeats(), Search.seans.getVipSeats(), Search.seans.getNormalPrice(), Search.seans.getVipPrice());
    }

    /**
     * Metoda sprawdzająca, czy wybrane miejsca na pewno są wolne (tj. czy w czasie od uruchomienia okna do kliknięcia "Przejdź dalej" inny użytkownik nie zajął miejsc)
     * @return true, jeśli zamówienie może zostać sfinalizowane, false, jeśli nie
     */
    static boolean checkSeansBeforeFinish(){
        for (int a: listaVip){
            for (int i = 0; i < Search.seans.getVipSeats().length; i++){
                if (!Search.seans.getVipSeats()[i] && a-1 == i) return false;
            }
        }
        for (int a: listaNormal){
            for (int i = 0; i < Search.seans.getNormalSeats().length; i++){
                if (!Search.seans.getNormalSeats()[i] && a-1 == i) return false;
            }
        }
        return true;
    }

    /**
     * Metoda wykonywana w momencie kliknięcia przycisku "Przejdź dalej" - finalizuje transakcję, generuje bilet i przesyła zamówienie na serwer.
     * @param imie imię użytkownika
     * @param nazwisko nazwisko zamawiającego
     * @param numer numer telefonu zamawiającego
     * @param email adres e-mail zamawiającego
     * @param cena kwota, którą klient zmuszony jest uiścić
     */
    static void enterButton(String imie, String nazwisko, String numer, String email, String cena){
        refresh();
        if (checkSeansBeforeFinish() && (!listaVip.isEmpty() || !listaNormal.isEmpty()) && !imie.isBlank() && !nazwisko.isBlank() && !email.isBlank() && !numer.isBlank() && Search.connected){
            PDDocument document = new PDDocument();
            PDPage page = new PDPage();
            document.addPage(page);
            PDPageContentStream contentStream = null;
            try {
                contentStream = new PDPageContentStream(document, page);
                String cinema = "KINO";
                InputStream stream = Choose.class.getResourceAsStream("/AbhayaLibre-Medium.ttf");
                PDFont font = PDType0Font.load(document, stream);

                float width = font.getStringWidth(cinema) / 1000 * 24;

                contentStream.setFont(font, 24);
                contentStream.beginText();
                contentStream.newLineAtOffset((page.getMediaBox().getWidth() - width)/2,page.getMediaBox().getHeight() - 50);
                contentStream.showText(cinema);
                contentStream.endText();

                PDImageXObject logo = PDImageXObject.createFromFile("src\\main\\resources\\logo.png", document);
                contentStream.drawImage(logo, (page.getMediaBox().getWidth() - logo.getWidth()) / 2, page.getMediaBox().getHeight() - logo.getHeight() - 75);

                //font = PDType1Font.TIMES_ROMAN;
                String film = seans.getFilm().getTitle();
                width = font.getStringWidth(film) / 1000 * 20;

                contentStream.setFont(font, 20);
                contentStream.beginText();
                contentStream.newLineAtOffset((page.getMediaBox().getWidth() - width)/2,page.getMediaBox().getHeight() - 375);
                contentStream.showText(film);
                contentStream.endText();

                SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                film = format1.format(seans.getDate().getTime());
                width = font.getStringWidth(film) / 1000 * 20;
                contentStream.beginText();
                contentStream.newLineAtOffset((page.getMediaBox().getWidth() - width)/2,page.getMediaBox().getHeight() - 400);
                contentStream.showText(film);
                contentStream.endText();

                format1 = new SimpleDateFormat("dd.MM");
                film = format1.format(seans.getDate().getTime());

                String miejscaVip = "Miejsca VIP: ";
                for (Integer i: listaVip) miejscaVip = miejscaVip + i.toString() + ", ";
                contentStream.setFont(font, 12);
                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 425);
                contentStream.showText(miejscaVip);
                contentStream.endText();

                String miejscaNormalne = "Miejsca normalne: ";
                for (Integer i: listaNormal) miejscaNormalne = miejscaNormalne + i.toString() + ", ";
                contentStream.setFont(font, 12);
                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 437);
                contentStream.showText(miejscaNormalne);
                contentStream.endText();

                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 450);
                contentStream.showText("Zamówione napoje i przekaski:");
                contentStream.endText();

                int ammount = 0;

                for(Product p: products){
                    if(p.getQuantity() > 0){
                        ammount++;
                        contentStream.beginText();
                        contentStream.newLineAtOffset(100,page.getMediaBox().getHeight() - 450 - ammount * 15);
                        contentStream.showText(p.getName() + " x" + p.getQuantity());
                        contentStream.endText();
                    }
                }

                ammount++;
                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 450 - ammount * 15);
                contentStream.showText("Cena: " + cena);
                contentStream.endText();

                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 625);
                contentStream.showText("Dane klienta:");
                contentStream.endText();

                contentStream.setFont(font, 18);
                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 650);
                contentStream.showText(imie + " " + nazwisko);
                contentStream.endText();

                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 675);
                contentStream.showText("Numer telefonu: " + numer);
                contentStream.endText();

                contentStream.beginText();
                contentStream.newLineAtOffset(50,page.getMediaBox().getHeight() - 700);
                contentStream.showText("Adres e-mail: " + email);
                contentStream.endText();

                contentStream.close();

                String myDocumentPath = System.getProperty("user.home") + "/Documents";

                document.save(new File(myDocumentPath + "/bilet_" + imie + "_" + nazwisko + "_" + film + ".pdf"));
                document.close();
                Integer[] listaN = new Integer[listaNormal.size()];
                for (int i = 0; i < listaN.length; i++){
                    listaN[i] = listaNormal.get(i) - 1;
                }
                Integer[] listaV = new Integer[listaVip.size()];
                for (int i = 0; i < listaV.length; i++){
                    listaV[i] = listaVip.get(i) - 1;
                }
                Product[] produkty = new Product[products.size()];
                for (int i = 0; i < produkty.length; i++){
                    produkty[i] = products.get(i);
                }

                Order order = new Order(
                        0L,
                        Search.seansIndex + 1L,
                        listaN,
                        listaV,
                        imie,
                        nazwisko,
                        numer,
                        email,
                        produkty,
                        Integer.parseInt(cena));
                Connection.postOrder(order);

            } catch (IOException e) {
                JFrame f = new JFrame();
                JOptionPane.showMessageDialog(f,"Błąd połączenia z serwerem", "Krytyczny błąd", JOptionPane.ERROR_MESSAGE);
            }

            boolean[] norm = new boolean[60];
            norm = seans.getNormalSeats();
            boolean[] vip = new boolean[20];
            vip = seans.getVipSeats();
            for (int i = 0; i < 20; i++){
                vip[i] = true;
            }
            for (int i = 0; i < 60; i++){
                norm[i] = true;
            }
            for (int i: listaNormal){
                norm[i-1] = false;
            }
            for (int i: listaVip){
                vip[i-1] = false;
            }
            for (int i = 0; i < 20; i++){
                System.out.println(vip[i]);
            }
            for (int i = 0; i < 60; i++){
                System.out.println(norm[i]);
            }
            seans.setVipSeats(vip);
            seans.setNormalSeats(norm);
            System.out.println(seans.display());
            JFrame f=new JFrame();
            JOptionPane.showMessageDialog(f,"Zgłoszenie wysłano, aplikacja powraca do menu");
            Search s = new Search();
            choose.dispose();
            choose = null;
        }else if (!checkSeansBeforeFinish()){
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f,"Podsiedzono cię", "Krytyczny błąd", JOptionPane.ERROR_MESSAGE);
        }else if (listaVip.isEmpty() && listaNormal.isEmpty()){
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f,"Nie zamówiłeś żadnych miejsc", "Krytyczny błąd", JOptionPane.ERROR_MESSAGE);
        }else if (imie.isBlank() || nazwisko.isBlank() || email.isBlank() || numer.isBlank()){
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f,"Nie dodałeś danych osobowych!", "Krytyczny błąd", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Metoda obliczająca kwotę do zapłaty przez zamawiającego
     * @return kwota w formacie String
     */
    static String price(){
        int cena = 0;
        for (Integer a: listaVip) cena += seans.getVipPrice();
        for (Integer a: listaNormal) cena += seans.getNormalPrice();
        for (Product p: products){
            cena += p.getQuantity() * p.getPrice();
        }
        String result = Integer.toString(cena);
        return result;
    }

    /**
     * Metoda generuje Stringa zawierającego listy wybranych miejsc sformatowane odpowiednio do wyświetlenia w polu tekstowym
     * @return listy miejsc w formacie String
     */
    static String getSeats(){
        String result = "Wybrane miejsca normalne: ";
        for (int b: listaNormal){
            result += Integer.toString(b) + " ";
        }
        result += "\n\nWybrane miejsca VIP: ";
        for (int b: listaVip){
            result += Integer.toString(b) + " ";
        }
        return result;
    }

    /**
     * Konstruktor tworzący okienko typu Choose
     * @param seans1 wybrany w oknie Search seans
     */
    Choose(Screening seans1){
        products.clear();
        listaNormal.clear();
        listaVip.clear();
        spinners.clear();
        try {
            products = Connection.getProducts();
        } catch (IOException e) {
            JFrame f = new JFrame();
            JOptionPane.showMessageDialog(f,"Błąd połączenia z serwerem", "Krytyczny błąd", JOptionPane.ERROR_MESSAGE);
        }
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        getContentPane().setBackground(Color.DARK_GRAY);
        Choose.seans = new Screening(seans1.getId(),seans1.getFilm(),seans1.getDate(),seans1.getNormalSeats(),seans1.getVipSeats(),seans1.getNormalPrice(),seans1.getVipPrice());
        setSize(900,850);
        JTextArea wybrane = new JTextArea();

        JTextField zaplata = new JTextField();
        zaplata.setEditable(false);
        zaplata.setFocusable(false);
        zaplata.setBounds(600,550,250,50);
        zaplata.setHorizontalAlignment(JTextField.CENTER);
        add(zaplata);

        try{
            for (Product p: products){
                int index = products.indexOf(p);
                JSpinner nazwa = new JSpinner();
                nazwa.setBounds(800,50 + index * 50,50,50);
                spinners.add(nazwa);
                add(nazwa);
            }

            for (int i = 0; i < 2; i++){
                for (int j = 0; j < 10; j++){
                    if (seans.getVipSeats()[i * 10 + j]){
                        JToggleButton button = new JToggleButton(Integer.toString(i * 10 + j + 1));
                        button.setBackground(Color.decode("#80ff80"));
                        button.setBounds(50 + j * 50,50 + i * 50,50,50);
                        button.setSelected(false);
                        button.addActionListener(e -> {
                            JToggleButton but = (JToggleButton) e.getSource();
                            int index = Integer.parseInt(but.getText());
                            if (but.isSelected()){
                                seans.getVipSeats()[index-1] = true;
                                listaVip.add(index);
                                listaVip.sort(Integer::compareTo);
                            }
                            else{
                                seans.getVipSeats()[index-1] = false;
                                listaVip.remove((Object)index);

                            }
                            for (Product p: products){
                                int ind = products.indexOf(p);
                                if ((int)spinners.get(ind).getValue() < 0) spinners.get(ind).setValue(0);
                                p.setQuantity((int)spinners.get(ind).getValue());
                            }
                            zaplata.setText(price());
                            wybrane.setText(getSeats());
                            wybrane.validate();
                        });
                        add(button);
                    }
                }
            }
            for (int i = 0; i < 6; i++){
                for (int j = 0; j < 10; j++){
                    if (seans.getNormalSeats()[i * 10 + j]){
                        JToggleButton button = new JToggleButton(Integer.toString(i * 10 + j + 1));
                        button.setBounds(50 + j * 50,150 + i * 50,50,50);
                        button.setSelected(false);
                        button.addActionListener(a -> {
                            JToggleButton but = (JToggleButton) a.getSource();
                            int index = Integer.parseInt(but.getText());
                            if (but.isSelected()){
                                seans.getNormalSeats()[index-1] = true;
                                listaNormal.add(index);
                                listaNormal.sort(Integer::compareTo);
                            }
                            else{
                                seans.getNormalSeats()[index-1] = false;
                                listaNormal.remove((Object)index);
                            }

                            for (Product p: products){
                                int ind = products.indexOf(p);
                                if ((int)spinners.get(ind).getValue() < 0) spinners.get(ind).setValue(0);
                                p.setQuantity((int)spinners.get(ind).getValue());
                            }
                            zaplata.setText(price());
                            wybrane.setText(getSeats());
                            wybrane.validate();
                        });
                        add(button);
                    }
                }
            }
        }catch (IndexOutOfBoundsException e){
            JFrame f=new JFrame();
            JOptionPane.showMessageDialog(f,"Pusta lista");
        }

        JTextField ekran = new JTextField("Ekran");
        ekran.setBounds(50,450,500,50);
        ekran.setEditable(false);
        ekran.setBackground(Color.LIGHT_GRAY);
        ekran.setHorizontalAlignment(JTextField.CENTER);
        wybrane.setEditable(false);
        wybrane.setBounds(50,500,500,100);
        wybrane.setMargin(new Insets(10,10,10,10));
        wybrane.setLineWrap(true);
        wybrane.setWrapStyleWord(true);
        add(ekran);
        add(wybrane);

        //JEDZENIE

        for(Product p: products){
            int index = products.indexOf(p);
            JTextField nazwa = new JTextField(p.getName() + " " + p.getPrice() + "zł/porcję");
            nazwa.setEditable(false);
            nazwa.setFocusable(false);
            nazwa.setBounds(600,50 + index * 50,200,50);
            nazwa.setHorizontalAlignment(JTextField.CENTER);
            add(nazwa);
        }

        JTextField zaplataLabel = new JTextField("Do zapłaty:");
        zaplataLabel.setEditable(false);
        zaplataLabel.setFocusable(false);
        zaplataLabel.setBounds(600,500,250,50);
        zaplataLabel.setHorizontalAlignment(JTextField.CENTER);
        add(zaplataLabel);

        ChangeListener changeListener = new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                for (Product p: products){
                    int index = products.indexOf(p);
                    if ((int)spinners.get(index).getValue() < 0) spinners.get(index).setValue(0);
                    p.setQuantity((int)spinners.get(index).getValue());
                }

                zaplata.setText(price());
            }
        };

        for(JSpinner spinner: spinners){
            spinner.addChangeListener(changeListener);
        }

        JTextField nameLabel = new JTextField("Imię");
        nameLabel.setBounds(50,625,320,25);
        nameLabel.setEditable(false);
        nameLabel.setFocusable(false);
        nameLabel.setBackground(Color.LIGHT_GRAY);
        nameLabel.setHorizontalAlignment(JTextField.CENTER);
        add(nameLabel);

        JTextField name = new JTextField("");
        name.setBounds(50,650,320,50);
        name.setEditable(true);
        name.setHorizontalAlignment(JTextField.CENTER);
        add(name);

        JTextField surnameLabel = new JTextField("Nazwisko");
        surnameLabel.setBounds(370,625,320,25);
        surnameLabel.setEditable(false);
        surnameLabel.setFocusable(false);
        surnameLabel.setBackground(Color.LIGHT_GRAY);
        surnameLabel.setHorizontalAlignment(JTextField.CENTER);
        add(surnameLabel);

        JTextField surname = new JTextField("");
        surname.setBounds(370,650,320,50);
        surname.setEditable(true);
        surname.setHorizontalAlignment(JTextField.CENTER);
        add(surname);

        JTextField mailLabel = new JTextField("Adres e-mail");
        mailLabel.setBounds(50,700,320,25);
        mailLabel.setEditable(false);
        mailLabel.setFocusable(false);
        mailLabel.setBackground(Color.LIGHT_GRAY);
        mailLabel.setHorizontalAlignment(JTextField.CENTER);
        add(mailLabel);

        JTextField mail = new JTextField("");
        mail.setBounds(50,725,320,50);
        mail.setEditable(true);
        mail.setHorizontalAlignment(JTextField.CENTER);
        add(mail);

        JTextField numberLabel = new JTextField("Numer telefonu");
        numberLabel.setBounds(370,700,320,25);
        numberLabel.setEditable(false);
        numberLabel.setFocusable(false);
        numberLabel.setBackground(Color.LIGHT_GRAY);
        numberLabel.setHorizontalAlignment(JTextField.CENTER);
        add(numberLabel);

        JTextField number = new JTextField("");
        number.setBounds(370,725,320,50);
        number.setEditable(true);
        number.setHorizontalAlignment(JTextField.CENTER);
        add(number);

        JButton enter = new JButton("Przejdź dalej");
        enter.setBounds(690,625,160,75);
        enter.setFocusable(false);
        enter.addActionListener(a -> {
            String imie = name.getText();
            String nazwisko = surname.getText();
            String email = mail.getText();
            String numer = number.getText();
            String cena = zaplata.getText();

            enterButton(imie, nazwisko, numer, email, cena);
        });
        add(enter);

        JButton returns = new JButton("Wstecz");
        returns.setBounds(690,700,160,75);
        returns.setFocusable(false);
        returns.addActionListener(a ->{
            Search s = new Search();
            choose.dispose();
            choose = null;
        });
        add(returns);

        setResizable(false);
        setVisible(true);
    }
}