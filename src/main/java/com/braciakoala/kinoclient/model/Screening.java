package com.braciakoala.kinoclient.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Klasa reprezentująca Seans filmowy w kinie
 */
public class Screening
{
    /**
     * Numer identyfikacyjny seansu
     */
    @JsonProperty("id")
    private int id;
    /**
     * Obiekt klasy Film odpowiadający danemu Seansowi
     */
    @JsonProperty("film")
    private Film film;
    /**
     * Data (w postaci tekstowej) seansu
     */
    @JsonProperty("date")
    private String date_string;
    /**
     * Data (w postaci Calender) seansu
     */
    @JsonIgnore
    private Calendar date;
    /**
     * Tablica booleanów reprezentująca wolne (true) oraz zajęte (false) miejsca normalne podczas seansu
     */
    @JsonProperty("normalSeats")
    private boolean[] normalSeats;
    /**
    * Tablica booleanów reprezentująca wolne (true) oraz zajęte (false) miejsca VIP podczas seansu
    */
    @JsonProperty("vipSeats")
    private boolean[] vipSeats;
    /**
     * Cena za rezerwacje jednego miejsca normalnego
     */
    @JsonProperty("normalPrice")
    private int normalPrice;
    /**
     * Cena za rezerwacje jednego miejsca VIP
     */
    @JsonProperty("vipPrice")
    private int vipPrice;

    public Screening() {
        id = 0;
        film = new Film();
        date_string = "2021-maj-11 07:48:38";
        date = Calendar.getInstance();
        normalSeats = new boolean[0];
        vipSeats = new boolean[0];
        normalPrice = -1;
        vipPrice = -1;
    }

    public Screening(Integer id, Film film, Calendar date, boolean[] normal, boolean[] vip, int normalPrice, int vipPrice){
        this.id = id;
        this.film = film;
        this.date = date;

        this.normalSeats = new boolean[normal.length];
        this.normalSeats = normal;

        this.vipSeats = new boolean[vip.length];
        this.vipSeats = vip;

        this.normalPrice = normalPrice;
        this.vipPrice = vipPrice;
    }

    public Calendar getDate() {
        return date;
    }
    //--------------------setters-------------------------------------

    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public void setDate_string(String date_string) {
        this.date_string = date_string;
    }

    public void setNormalSeats(boolean[] normalSeats) {
        this.normalSeats = normalSeats;
    }

    public void setVipSeats(boolean[] vipSeats) {
        this.vipSeats = vipSeats;
    }

    public void setNormalPrice(int normalPrice) {
        this.normalPrice = normalPrice;
    }

    public void setVipPrice(int vipPrice) {
        this.vipPrice = vipPrice;
    }

    //------------------getters-----------------------------------
    public String getDate_string() {
        return date_string;
    }

    public Film getFilm() {
        return film;
    }

    public boolean[] getNormalSeats() {
        return normalSeats;
    }

    public boolean[] getVipSeats() {
        return vipSeats;
    }

    public int getNormalPrice() {
        return normalPrice;
    }

    public int getVipPrice() {
        return vipPrice;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "SeansResponse{" +
                "id=" + id +
                ", film=" + film +
                '}';
    }

    public String display() {
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM HH:mm");
        String czas = format1.format(date.getTime());
        String result = czas + "\n\n" + film.display();
        int licznik = 0;
        for (boolean a: normalSeats){
            if (a) licznik++;
        }
        result += "\n\nWolne miejsca normalne: " + licznik;
        licznik = 0;
        for (boolean a: vipSeats){
            if (a) licznik++;
        }
        result += "\n\nWolne miejsca vip: " + licznik;
        result += "\n\nNormalny bilet: " + this.normalPrice + "zł";
        result += "\n\nVIPowski bilet: " + this.vipPrice + "zł";
        return result;
    }
}
