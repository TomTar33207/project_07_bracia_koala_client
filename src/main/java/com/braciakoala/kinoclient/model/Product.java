package com.braciakoala.kinoclient.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Klasa reprezentująca produkt możliwy do kupienia w kinie
 */
@RestResource(exported = false)
public class Product {
    /**
     * Numer identyfikacyjny produktu
     */
    @JsonProperty("id")
    private Long id;
    /**
     * Nazwa produktu
     */
    @JsonProperty("name")
    private String name;
    /**
     * Cena za sztukę produktu
     */
    @JsonProperty("price")
    private int price;
    /**
     * Ilość sztuk produktu
     */
    @JsonProperty("quantity")
    private int quantity;

    public Product() { }

    public Product(Long id, String name, int price, int quantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
