package com.braciakoala.kinoclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Klasa reprezentująca zamówienie w kinie
 */
public class Order
{
    /**
     * Numer identyfikacyjny zamówienia
     */
    private Long id;
    /**
     * Numer identyfikacyjny filmu przypisanego do seansu
     */
    private Long screeningId;

    /**
     * Tablica z liczbami odpowiadającymi zarezerwowanym miejscom normalnym
     */
    private Integer[] normalSeats;

    /**
     * Tablica z liczbami odpowiadającymi zarezerwowanym miejscom VIP
     */
    private Integer[] vipSeats;

    /**
     * Imie osoby rezerwującej
     */
    private String name;
    /**
     * Nazwisko osoby rezerwującej
     */
    private String surname;
    /**
     * Numer komórkowy osoby rezerwującej
     */
    private String phoneNumber;

    /**
     * Adres E-mail osoby rezerwującej
     */
    private String emailAddress;

    /**
     * Tablica produktów które zostały zamówione
     */
    private Product[] products;
    /**
     * Koszt zamówienia
     */
    private int cost;

    public Order() { }

    public Order(
            Long id,
            Long screeningId,
            Integer[] normalSeats,
            Integer[] vipSeats,
            String name,
            String surname,
            String phoneNumber,
            String emailAddress,
            Product[] products,
            int cost) {
        this.id = id;
        this.screeningId = screeningId;
        this.normalSeats = normalSeats;
        this.vipSeats = vipSeats;
        this.name = name;
        this.surname = surname;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.products = products;
        this.cost = cost;
    }
//------------------GETTERS----------------------------
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public int getCost() {
        return cost;
    }

    public Long getScreeningId() {
        return screeningId;
    }

    public Integer[] getNormalSeats() {
        return normalSeats;
    }

    public Integer[] getVipSeats() {
        return vipSeats;
    }

    public Product[] getProducts() {
        return products;
    }


    public void setId(Long id) {
        this.id = id;
    }
    //------------------SETTERS----------------------------

    public void setScreeningId(Long screeningId) {
        this.screeningId = screeningId;
    }

    public void setNormalSeats(Integer[] normalSeats) {
        this.normalSeats = normalSeats;
    }

    public void setVipSeats(Integer[] vipSeats) {
        this.vipSeats = vipSeats;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    //------------------OTHER------------------------------
    @Override
    public String toString() {
        String result;
        result = id + "," + screeningId + ",";

        if (normalSeats.length != 0) {
            for (Integer seat : normalSeats) {
                result += seat + ";";
            }
            result = result.substring(0, result.length() - 1);
        }
        result += ",";
        if (vipSeats.length != 0) {
            for (Integer seat : vipSeats) {
                result += seat + ";";
            }
            result = result.substring(0, result.length() - 1);
        }

        result += "," + name + "," + surname + "," + phoneNumber + "," + emailAddress + ",";

        for (Product productDto : products) {
            result += productDto.getId() + ";" + productDto.getQuantity() + "_";
        }
        result = result.substring(0, result.length() - 1);

        result += "," + cost;

        return result;
    }
}