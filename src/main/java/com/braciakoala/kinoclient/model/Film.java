package com.braciakoala.kinoclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Klasa reprezentująca film;
 */
@RestResource(exported = false)
public class Film
{
    /**
     * Numer identyfikacyjny
     */
    private Long id;
    /**
     * Tytuł filmu
     */
    private String title;
    /**
     * Opis filmu
     */
    private String description;
    /**
     * Rok produkcji Filmu
     */
    private int yearOfProduction;
    /**
     * Gatunek filmu
     */
    private String genre;

    public Film() {
        this.id = id;
        this.title = "";
        this.description = "";
        this.yearOfProduction = -1;
        this.genre = "";
    }

    public Film(Long id, String title, String desc, int year, String genre){
        this.id = id;
        this.title = title;
        this.description = desc;
        this.yearOfProduction = year;
        this.genre = genre;
    }
    public Film(Film film){
        this.id = film.getId();
        this.title = film.getTitle();
        this.description = film.getDescription();
        this.yearOfProduction = film.getYear_int();
        this.genre = film.getGenre();
    }

 //-----------------------GETTERS----------------------------

    /**
     * Metoda zwracająca numer identyfikacyjny filmu.
     * @return numer identyfikacyjny filmu.
     */
    public Long getId() {
        return id;
    }

    /**
     * Metoda zwracająca gatunek filmu.
     * @return Gatunek filmu.
     */
     public String getGenre(){
        return genre;
    }
    /**
     * Metoda zwracająca tytuł filmu.
     * @return Tytuł filmu.
     */
    public String getTitle(){
        return title;
    }
    /**
     * Metoda zwracająca opis filmu.
     * @return Opis filmu.
     */
    public String getDescription(){
        return description;
    }
    /**
     * Metoda zwracająca rok produkcji.
     * @return Rok produkcji filmu.
     */
    public String getYear(){
        return Integer.toString(yearOfProduction);
    }
    /**
     * Metoda zwracająca numer identyfikacyjny filmu.
     * @return Numer identyfikacyjny filmu.
     */
    int getYear_int(){ return yearOfProduction; }
    //--------------------SETTERS-------------------------------

    /**
     * Metoda ustawiająca opis filmu na ten podany jako argument metody.
     * @param description Nowy opis filmu.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Metoda ustawiająca numer identyfikacyjny filmu na ten podany jako argument metody.
     * @param id Nowy numer identyfikacyjny.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Metoda ustawiająca tytuł filmu na ten podany jako argument metody.
     * @param title Nowy tytuł filmu.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Metoda ustawiająca rok produkjci filmu na ten podany jako argument metody.
     * @param yearOfProduction rok produkcji
     */
    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }
    /**
     * Metoda ustawiająca gatunek filmu na ten podany jako argument metody.
     * @param genre gatunek filmu
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    //--------------------OTHERS-----------------------------
    @Override
    public String toString() {
        return "FilmResponse{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                ", genre='" + genre + '\'' +
                '}';
    }

    /**
     * Metoda zwracająca Stringa z informacjami o filmie - sformatowanymi w sposób odpowiedni do wyświetlenie w oknie opisu
     * @return
     */
    public String display(){
        String result = title + ", " + Integer.toString(yearOfProduction) + "\n\n" + genre + "\n\n" + description;
        return result;
    }
}