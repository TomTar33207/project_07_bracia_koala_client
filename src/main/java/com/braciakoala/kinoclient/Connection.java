package com.braciakoala.kinoclient;

import com.braciakoala.kinoclient.model.Order;
import com.braciakoala.kinoclient.model.Product;
import com.braciakoala.kinoclient.model.Screening;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Klasa reprezentująca połączenie z serverem, posiada statyczna zmienna URL, oraz statyczne metody pozwalające obsługiwać serwer.
 */
public class Connection {
    /**
     * URL do połączenia z serverem
     */
    private static String URL = "http://localhost:8080/api/";
    /**
     * Statyczny obiekt ObjectMappera służący do zamiany JSON na obiekty klas.
     */
    private static ObjectMapper objectMapper = new ObjectMapper();

    //-----------------------------GETER----------------------------------------------

    /**
     * Metoda łącząca się z serverem i pobierająca seans o id podanym jako parametr
     * @param seanseId Id seansu który chcemy pobrać
     * @return Metoda zwraca pojedyńczy obiekt klasy Screening
     * @throws IOException W razie niepowodzenia zwracany jest null;
     */
    public static Screening getSeanse(Integer seanseId) throws IOException {
        try {
            Screening screening = objectMapper.readValue(new URL(URL + "repertoire/screening/" + seanseId), Screening.class);
            addCalendarData(screening);
            return screening;
        }catch (Exception e){
            return null;
        }
    }
    /**
     * Metoda łącząca się z serverem i pobierająca liste dostępnych seansów.
     * @return Metoda zwraca liste wszystkich seansów.
     * @throws IOException W razie niepowodzenia zwracana jest pusta lista.
     */
    public static ArrayList<Screening> getAllSeanse() throws IOException {

        ArrayList<Screening> listOfSeans = new ArrayList<Screening>();
        try{
            String json = goHttpGet("repertoire/get-all-screenings");

            listOfSeans = objectMapper.readValue(json, new TypeReference<ArrayList<Screening>>() {});

            for (Screening screening:listOfSeans)
                addCalendarData(screening);

            return listOfSeans;
        } catch (IOException e) {
            System.out.println("GET AllSeanse PROBLEM");
            return listOfSeans;
        }


    }

    /**
     * Metoda łącząca się z serverem i pobierająca liste dostępnych Produktów.
     * @return Metoda zwraca liste wszystkich produktów.
     * @throws IOException W razie niepowodzenia zwracana jest pusta lista.
     */
    public static ArrayList<Product> getProducts() throws IOException {

        ArrayList<Product> products = new ArrayList<Product>();
        try{
            String json = goHttpGet("orders/get-all-products");
            products = objectMapper.readValue(json, new TypeReference<ArrayList<Product>>() {});
            return products;
        } catch (IOException e) {
            System.out.println("GET Products PROBLEM");
            return products;

        }
    }
    //-----------------------------POSTS---------------------------------------------

    /**
     * Metoda wysyła na server informacje o zamowieniu podanym jako parametr.
     * @param newOrder jest to zamówienie które jest wysyłane na server.
     * @return Metoda zwraca kod odpowiedzi z servera
     * @throws IOException Problem z outputem/inputem - zwróć 0
     */
    public static int postOrder(Order newOrder) {

        try{
            final CloseableHttpClient client = HttpClients.createDefault();
            final HttpPost httpPost = new HttpPost(URL + "orders/create-order");
            String json = objectMapper.writeValueAsString(newOrder);
            final StringEntity entity = new StringEntity(json,"UTF-8");
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            final CloseableHttpResponse response = client.execute(httpPost);
            client.close();
            return response.getStatusLine().getStatusCode();
        }catch (IOException e) {
            System.out.println("Problem with POST input output");
            return 0;
        }


    }

    //-----------------------------PUT-----------------------------------------------

    /**
     * Metoda ustawiająca seans na serwerze o podanym id na podany jako drugi argument.
     * @param id id seansu ktory chcemy edytować.
     * @param newSeanse obiekt który zastępuje seans na serwerze.
     * @return Zrwacana jest wartosc kodu odpowiedzi servera.
     * @throws IOException Problem z outputem/inputem - zwróć 0
     */
    public static int putSeanse(int id, Screening newSeanse) throws UnsupportedEncodingException, JsonProcessingException {

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPut httpPut = new HttpPut(URL + "repertoire/screening-edit/" + id);
        String json = objectMapper.writeValueAsString(newSeanse);
        final StringEntity entity = new StringEntity(json,"UTF-8");

        try{
            httpPut.setEntity(entity);
            httpPut.setHeader("Content-type", "application/json");
            final CloseableHttpResponse response = client.execute(httpPut);
            client.close();
            return response.getStatusLine().getStatusCode();

        } catch (IOException e) {
            System.out.println("Problem with PUT input output");
            return 0;
        }

    }

    //-----------------------------TEST----------------------------------------------

    /**
     * Metoda pobierająca przykladowe zamówienie z serwera - do testów
     * @return przykladowe zamówienie z serwera
     * @throws IOException w razie niepowodzenia zwraca null
     */
    public static Order getTestOrder() throws IOException {
        try{
            Order order = objectMapper.readValue(new URL(URL + "orders/order-test"), Order.class);
            System.out.println(order.toString());
            return order;
        }catch (Exception e){
            return null;
        }


    }
    //-----------------------------OTHER------------------------------------------

    /**
     * Metoda przypisuje obiektowi typu Screening kalendarzową datę jako typ Calendar.
     * @param screening Obiekt typu Screening który nalerzy obsłużyć.
     */
    private static void addCalendarData(Screening screening)
    {
        Calendar newDate = screening.getDate();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            newDate.setTime(sdf.parse(screening.getDate_string()));
            screening.setDate(newDate);
        } catch (Exception exception) {
            System.out.println("Convert date to calendar format problem!! ");
        }
    }

    /**
     *
     * @param url Scieżka do interesującej nas metody.
     * @return JSON pobrany z serwera w postaci Stringa.
     * @throws IOException W razie niepowodzenia zwracany zostaje wyjątek.
     */
    private static String goHttpGet(String url) throws IOException {

        final HttpClient client = HttpClientBuilder.create().build();
        final HttpGet request = new HttpGet(URL + url);
        final HttpResponse response = client.execute(request);
        final HttpEntity entity = response.getEntity();
        final String json = EntityUtils.toString(entity, HTTP.UTF_8);

        return json;

    }
}